#include <windows.h>
#include <time.h>
#include <stdlib.h>

HDC desk;
static int sw, sh;

void GetDesk() {
	desk = GetDC(0);
	sw = GetSystemMetrics(0);
	sh = GetSystemMetrics(1);
}

DWORD WINAPI Invert(LPVOID lpParam) {
	for (;;) {
		GetDesk();
		BitBlt(desk, 0, 0, sw, sh, desk, 0, 0, NOTSRCCOPY);
		ReleaseDC(0, desk);
		//Sleep(100);
	};
}
DWORD WINAPI Melter(LPVOID lpParam) {
	int l, t, w, h;

	for (;;) {
		GetDesk();
		l = rand() % sw;
		t = rand() % sh;
		w = 256;
		h = 256;
		BitBlt(desk, l + (((rand() % 6) - 1) * 16), t + (((rand() % 6) - 1) * 16), w, h, desk, l, t, SRCCOPY);
		ReleaseDC(0, desk);
		//Sleep(100);
	};
}

int main() {
	srand(time(0));
	ShowWindow(GetForegroundWindow(), SW_HIDE);
	CreateThread(0, 0, Invert, 0, 0, 0);
	CreateThread(0, 0, Melter, 0, 0, 0);
	Sleep(115706880000); //1year
	return 0;
}
