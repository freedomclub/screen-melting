#include <stdlib.h>
#include <windows.h>
#include <time.h>

HBRUSH bruh;

HDC desk;

static int sw, sh;

DWORD WINAPI DeskRefresher(LPVOID lpParam) {
	for (;;) {
		Sleep(1000);
		RedrawWindow(0, 0, 0, RDW_ALLCHILDREN | RDW_ERASE | RDW_INVALIDATE);
	};
}

void MakeSolidBrush() {
	bruh = CreateSolidBrush(RGB(rand() % 256, rand() % 256, rand() % 256));
	SelectObject(desk, bruh);
}

void MakeHatchBrush() {
	switch (rand() % 6) {
	case 0: bruh = CreateHatchBrush(HS_BDIAGONAL, RGB(rand() % 256, rand() % 256, rand() % 256)); break;
	case 1: bruh = CreateHatchBrush(HS_CROSS, RGB(rand() % 256, rand() % 256, rand() % 256)); break;
	case 2: bruh = CreateHatchBrush(HS_DIAGCROSS, RGB(rand() % 256, rand() % 256, rand() % 256)); break;
	case 3: bruh = CreateHatchBrush(HS_FDIAGONAL, RGB(rand() % 256, rand() % 256, rand() % 256)); break;
	case 4: bruh = CreateHatchBrush(HS_HORIZONTAL, RGB(rand() % 256, rand() % 256, rand() % 256)); break;
	case 5: bruh = CreateHatchBrush(HS_VERTICAL, RGB(rand() % 256, rand() % 256, rand() % 256)); break;
	};
	SelectObject(desk, bruh);
}

DWORD WINAPI Psychedelic(LPVOID lpParam) {

	desk = GetDC(0);
	sw = GetSystemMetrics(0);
	sh = GetSystemMetrics(1);

	for (;;) {
		GetDesk();
		MakeSolidBrush();
		switch (rand() % 3) {
		case 0: PatBlt(desk, 0, 0, sw, sh, PATINVERT); break;
		case 1: PatBlt(desk, 0, 0, sw, sh, PATINVERT); break;
		case 2: PatBlt(desk, 0, 0, sw, sh, PATINVERT); break;
		};
		Sleep(2);
		MakeHatchBrush();
		switch (rand() % 3) {
		case 0: PatBlt(desk, 0, 0, sw, sh, PATINVERT); break;
		case 1: PatBlt(desk, 0, 0, sw, sh, PATINVERT); break;
		case 2: PatBlt(desk, 0, 0, sw, sh, PATINVERT); break;
		};
		ReleaseDC(0, desk);
		Sleep(2);
	};
}

int main() {
	srand((unsigned int)time(0));
	CreateThread(0, 0, Psychedelic, 0, 0, 0);
	CreateThread(0, 0, DeskRefresher, 0, 0, 0);
	Sleep(-1);
}
