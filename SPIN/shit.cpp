#include <windows.h>
#include <stdlib.h>
#include <time.h>

HDC desk;
static int sw, sh;
POINT pepe[4];

void GetDesk() {
	desk = GetDC(0);
	sw = GetSystemMetrics(0);
	sh = GetSystemMetrics(1);
}

DWORD WINAPI MirrorV(LPVOID lpParam) {
	for (;;) {
		GetDesk();
		pepe[0].x = 0;
		pepe[0].y = sh;
		pepe[1].x = sw;
		pepe[1].y = sh;
		pepe[2].x = 0;
		pepe[2].y = 0;
		pepe[3].x = sw;
		pepe[3].y = 0;
		PlgBlt(desk, pepe, desk, 0,0,sw,sh,0,0,0);
		ReleaseDC(0, desk);
		Sleep(70);
	}
}

DWORD WINAPI MirrorH(LPVOID lpParam) {
	for (;;) {
		GetDesk();
		pepe[0].x = sw;
		pepe[0].y = 0;
		pepe[1].x = 0;
		pepe[1].y = 0;
		pepe[2].x = sw;
		pepe[2].y = sh;
		pepe[3].x = 0;
		pepe[3].y = sh;
		PlgBlt(desk, pepe, desk, 0, 0, sw, sh, 0, 0, 0);
		ReleaseDC(0, desk);
		Sleep(70);
	}
}

int main(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow) {
	srand(time(0));
	CreateThread(0, 0, MirrorH, 0, 0, 0);
	CreateThread(0, 0, MirrorV, 0, 0, 0);
	Sleep(-1);
	return 0;
}
