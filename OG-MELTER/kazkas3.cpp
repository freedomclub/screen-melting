#include <Windows.h>

#pragma warning(disable:4996)
#pragma warning(disable:4703)

static int ScreenWidth, ScreenHeight;

LRESULT CALLBACK Melter(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	HDC Window = GetDC(hWnd);

	//BitBlt(Window, 0, 0, ScreenWidth, ScreenWidth, Window, 0, 0, NOTSRCCOPY); // flashing lights

	switch (Msg){
		case WM_CREATE:
		{
			HDC Desktop = GetDC(HWND_DESKTOP);

			BitBlt(Window, 0, 0, ScreenWidth, ScreenHeight, Desktop, 0, 0, SRCCOPY);
			ReleaseDC(hWnd, Window);
			ReleaseDC(HWND_DESKTOP, Desktop);

			SetTimer(hWnd, 0, 50, 0); // time
			ShowWindow(hWnd, SW_SHOW);
			break;
		}
		case WM_PAINT:
		{
			ValidateRect(hWnd, 0);
			break;
		}
		case WM_TIMER:
		{
			int X = (rand() % ScreenWidth) - (150 / 2), // 150
				Y = (rand() % 15),
				Width = (rand() % 150); // 150
			BitBlt(Window, X, Y, Width, ScreenHeight, Window, X, 0, SRCCOPY);
			ReleaseDC(hWnd, Window);
			break;
		}
		case WM_DESTROY:
		{
			KillTimer(hWnd, 0);
			PostQuitMessage(0);
			break;
		}
		default:
			break;
		return 0;
	}
	return DefWindowProc(hWnd, Msg, wParam, lParam);
}

int main(HINSTANCE Inst, HINSTANCE Prev, LPSTR Cmd, int showcmd)
{
	// Get the width & height of current display
	ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	ScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	WNDCLASS wndClass = { 0, Melter, 0,0, Inst, 0, LoadCursorW(0, IDC_ARROW), 0, 0, L"ScreenMelter" };

	if (RegisterClass(&wndClass))
	{
		// Create the "melter" overlapping window.
		HWND hWnd = CreateWindowExA(WS_EX_TOPMOST, "ScreenMelter", 0, WS_POPUP,
			0, 0, ScreenWidth, ScreenHeight, HWND_DESKTOP, 0, Inst, 0);
		if (hWnd)
		{
			// seed for randomization
			srand(GetTickCount64());
			MSG Msg = { 0 };
			// Run the melter loop
			while (Msg.message != WM_QUIT)
			{
				if (PeekMessage(&Msg, 0, 0, 0, PM_REMOVE))
				{
					TranslateMessage(&Msg);
					DispatchMessage(&Msg);
				}
			}
		}
	}
	return 0;
}